<?php

use yii\base\Component;
use yii\BaseYii;

/**
 * Class Yii
 */
class Yii extends BaseYii
{
    /**
     * @var Web
     */
    public static $app;
}

/**
 * Class Web
 * @method \yii\web\Request getRequest()
 * @method \yii\web\Response getResponse()
 */
class Web extends Component
{
}
