<?php

namespace semako\userVk\enums;

/**
 * Class TableName
 * @package semako\userVk\enums
 */
abstract class TableName
{
    const TOKEN = 'token';
    const USER  = 'user';
}
