<?php

namespace semako\userVk\behaviors;

use semako\userVk\models\Token;
use semako\userVk\traits\Token as TokenTrait;
use yii;
use yii\base\ActionFilter;
use yii\web\UnauthorizedHttpException;

/**
 * Class TokenAuth
 * @package app\components
 */
class TokenAuth extends ActionFilter
{
    /**
     * @param yii\base\Action $action
     * @return bool
     * @throws UnauthorizedHttpException
     */
    public function beforeAction($action)
    {
        if (Yii::$app->getRequest()->getMethod() == 'OPTIONS') {
            Yii::$app->getResponse()->data = [];
            Yii::$app->getResponse()->statusCode = 200;
            Yii::$app->getResponse()->send();
            return false;
        }

        /** @var TokenTrait $owner */
        $owner = $this->owner;
        $token = $owner->getHeaderToken();

        if (!$token) {
            throw new UnauthorizedHttpException();
        }

        /** @var Token $modelToken */
        $modelToken = Token::find()->byToken($token)->isNotExpired()->one();

        if (!$modelToken) {
            throw new UnauthorizedHttpException();
        }

        $owner->setToken($modelToken);

        return parent::beforeAction($action);
    }
}
