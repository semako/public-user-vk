<?php

namespace semako\userVk\migrations;

use semako\userVk\enums\TableName;
use yii\db\Migration;

/**
 * Class m000001_000001_user_and_token
 */
class m000001_000001_user_and_token extends Migration
{
    /**
     *
     */
    public function safeUp()
    {
        $this->createTable(TableName::USER, [
            'id'       => $this->primaryKey(20),
            'id_vk'    => $this->bigInteger(20)->defaultValue(null),
            'login'    => $this->string(128)->defaultValue(null),
            'password' => $this->string(40)->defaultValue(null),
        ]);

        $this->createTable(TableName::TOKEN, [
            'id'         => $this->primaryKey(20),
            'id_user'    => $this->bigInteger(20)->notNull(),
            'token'      => $this->string(128)->notNull(),
            'token_vk'   => $this->string(128)->defaultValue(null),
            'expire_at'  => $this->bigInteger(20)->notNull(),
            'created_at' => $this->bigInteger(20)->notNull(),
        ]);
    }

    /**
     *
     */
    public function safeDown()
    {
        $this->dropTable(TableName::USER);
        $this->dropTable(TableName::TOKEN);
    }
}
