<?php

namespace semako\userVk\traits;

use semako\userVk\models\Token as TokenModel;
use yii;
use yii\web\UnauthorizedHttpException;

/**
 * Class Token
 * @package semako\userVk\traits
 */
trait Token
{
    /**
     * @var TokenModel|null
     */
    private $token;

    /**
     * @return string
     */
    public function getHeaderToken()
    {
        return Yii::$app->getRequest()->getHeaders()->get('x-token');
    }

    /**
     * @param TokenModel $token
     */
    public function setToken(TokenModel $token)
    {
        $this->token = $token;
    }

    /**
     * @return TokenModel
     * @throws UnauthorizedHttpException
     */
    public function getToken()
    {
        if (!$this->token) {
            throw new UnauthorizedHttpException();
        }

        return $this->token;
    }
}
