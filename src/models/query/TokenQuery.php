<?php

namespace semako\userVk\models\query;

use semako\userVk\models\User;
use semako\yii2Common\components\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\semako\userVk\models\Token]].
 * @see \semako\userVk\models\Token
 */
class TokenQuery extends ActiveQuery
{
    /**
     * @param User $user
     * @return $this
     */
    public function byUser(User $user)
    {
        return $this->andWhere([
            'id_user' => $user->id,
        ]);
    }

    /**
     * @param $token
     * @return $this
     */
    public function byToken($token)
    {
        return $this->andWhere([
            'token' => $token,
        ]);
    }

    /**
     * @return $this
     */
    public function isNotExpired()
    {
        return $this->andWhere(['>', 'expire_at', time()]);
    }
}
