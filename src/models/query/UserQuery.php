<?php

namespace semako\userVk\models\query;

use semako\yii2Common\components\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\semako\userVk\models\User]].
 * @see \semako\userVk\models\User
 */
class UserQuery extends ActiveQuery
{
    /**
     * @param $idVk
     * @return $this
     */
    public function byIdVk($idVk)
    {
        return $this->andWhere([
            'id_vk' => $idVk,
        ]);
    }
}
