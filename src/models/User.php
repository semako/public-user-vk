<?php

namespace semako\userVk\models;

use semako\userVk\enums\TableName;
use semako\userVk\models\query\UserQuery;
use semako\yii2Common\components\ActiveRecord;
use yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property integer $id_vk
 * @property string $login
 * @property string $password
 */
class User extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%' . TableName::USER . '}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_vk'], 'integer'],
            [['login'], 'string', 'max' => 128],
            [['password'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_vk' => Yii::t('app', 'Id Vk'),
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * @inheritdoc
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }
}
