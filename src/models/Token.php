<?php

namespace semako\userVk\models;

use semako\userVk\enums\TableName;
use semako\userVk\models\query\TokenQuery;
use semako\yii2Common\components\ActiveRecord;
use yii;

/**
 * This is the model class for table "{{%token}}".
 *
 * @property integer $id
 * @property integer $id_user
 * @property string $token
 * @property string $token_vk
 * @property integer $expire_at
 * @property integer $created_at
 */
class Token extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%' . TableName::TOKEN . '}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'token', 'expire_at', 'created_at'], 'required'],
            [['id_user', 'expire_at', 'created_at'], 'integer'],
            [['token', 'token_vk'], 'string', 'max' => 128],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Id User'),
            'token' => Yii::t('app', 'Token'),
            'token_vk' => Yii::t('app', 'Token Vk'),
            'expire_at' => Yii::t('app', 'Expire At'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @inheritdoc
     *
     * The default implementation returns the names of the columns whose values have been populated into this record.
     */
    public function fields()
    {
        return [
            'token'  => 'token',
            'expire' => 'expire_at',
        ];
    }

    /**
     * @inheritdoc
     * @return TokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TokenQuery(get_called_class());
    }

    /**
     * @return User|array|null
     */
    public function getUser()
    {
        return User::find()->byPk($this->id_user)->one();
    }
}
